package jving.ltu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class LiveData implements Runnable{
	
	public float[][] values;
	String baseurl = "http://www.eislab.com/weather/get_data.php?s="; // append: 2012-09-20_23:30
	String TAG = "LiveData";
	public double[] phase;
	public float maxv = 1;
	private int[] tsteps;
	private String dformat = "yyyy-MM-dd'_'HH:mm";

	@Override
	public void run() {
		// TODO Auto-generated method stub


    	StringBuilder builder = new StringBuilder();
    	HttpClient client = new DefaultHttpClient();
    	
    	SimpleDateFormat sdf = new SimpleDateFormat(dformat, Locale.ENGLISH);
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.DATE, -1); 
    	
    	Log.d(TAG,"http://www.eislab.com/weather/get_data.php?s=2012-09-20_23:30");
    	Log.d(TAG,baseurl + sdf.format(cal.getTime()));
    	
    	HttpGet httpGet = new HttpGet(baseurl + sdf.format(cal.getTime()));//+dateTime); // Maybe do a sanity check?
    	
		try {
	    	HttpResponse response = client.execute(httpGet);
	        StatusLine statusLine = response.getStatusLine();
	        int statusCode = statusLine.getStatusCode();
	        if (statusCode == 200) {
	          HttpEntity entity = response.getEntity();
	          InputStream content;
			  content = entity.getContent();
			  BufferedReader reader = new BufferedReader(new InputStreamReader(content));
	          String line;
			  while ((line = reader.readLine()) != null) {
				    builder.append(line);
			  }
	        } else {
	            Log.e(TAG, "Failed to download consumption");
	        }
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	//String tempData = "{\"error\":false,\"values\":[{\"timestamp\":1346603989,\"1\":0.1519,\"2\":0.8019,\"3\":0},{\"timestamp\":1346604054,\"1\":0.1519,\"2\":0.8069,\"3\":0},{\"timestamp\":1346604247,\"1\":0.1519,\"2\":0.804,\"3\":0},{\"timestamp\":1346604312,\"1\":0.1519,\"2\":0.8119,\"3\":0},{\"timestamp\":1346604376,\"1\":0.1529,\"2\":0.8059,\"3\":0},{\"timestamp\":1346604440,\"1\":0.1519,\"2\":0.813,\"3\":0},{\"timestamp\":1346604474,\"1\":0.1789,\"2\":0.8019,\"3\":0},{\"timestamp\":1346604539,\"1\":0.178,\"2\":0.903,\"3\":0},{\"timestamp\":1346604735,\"1\":0.177,\"3\":0},{\"timestamp\":1346604866,\"2\":0.798,\"3\":0,\"1\":0.5559},{\"timestamp\":1346604995,\"2\":0.8019,\"3\":0,\"1\":0.1519},{\"timestamp\":1346605061,\"3\":0,\"1\":0.1519,\"2\":0.8},{\"timestamp\":1346605127,\"3\":0,\"1\":0.1529,\"2\":0.8019}]}";
    	double[] valus = new double[3];
    	try {
			JSONObject jsonO = new JSONObject(builder.toString());
			//Log.d(TAG,"Number of entries " + jsonO.length());
			JSONObject err = jsonO.getJSONObject("error");
			JSONArray jsoArr = jsonO.getJSONArray("values");  
			values = new float[3][jsoArr.length()];
			phase = new double[3];
			phase[0] = 0;
			phase[1] = 0;
			phase[2] = 0;
			for (int i = 0; i< jsoArr.length(); i++ ) {
				JSONObject jso = jsoArr.getJSONObject(i);
				//Log.d(TAG,"  Json: " + jso.optString("timestamp") + "," + String.valueOf(jso.optDouble("1")) + "," + String.valueOf(jso.optDouble("2")) + "," + String.valueOf(jso.optDouble("3")));
				tsteps[i] = jso.optInt("timestamp");
				valus[0] = (double) jso.optDouble("1");
				valus[1] = (double) jso.optDouble("2");
				valus[2] = (double) jso.optDouble("3");
				for (int j = 0; j < 3; j++) {
					if ( Double.isNaN(valus[j]) ) { //Not detecting NaN... GAH!
						valus[j] = 0;
						//Log.d(TAG,"Got NaN");
					}
				}
				if (i > 1) {
					phase[0] += valus[0]*(tsteps[i]-tsteps[i-1])/60/60; // Get it into kWh
					phase[1] += valus[1]*(tsteps[i]-tsteps[i-1])/60/60; // Get it into kWh
					phase[2] += valus[2]*(tsteps[i]-tsteps[i-1])/60/60; // Get it into kWh
				}
				values[0][i] = (float) valus[0];
				values[1][i] =(float) valus[1];
				values[2][i] =(float) valus[2];
				
				maxv = (float) Math.max(valus[0], maxv);
				maxv = (float) Math.max(valus[1], maxv);
				maxv = (float) Math.max(valus[2], maxv);
				}
			//Log.d(TAG,"Phases: " + String.valueOf(phase[0]) + "," + String.valueOf(phase[1]) + "," + String.valueOf(phase[2]) + ",");
		} catch (JSONException je) {
			// TODO Auto-generated catch block
			je.printStackTrace();
		}
    	
    	MainActivity.updateAllValues();
	}
	

}