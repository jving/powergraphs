package jving.ltu;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {
	/*
	 * TODO: Fix graphs so that they have numbers
	 * TODO: Add "fixed price" and 2* nordpol
	 * TODO: Add "price for period"  
	 * 
	 */
	
	

    private static final int DRAG = 0;
	private static final int ZOOM = 1;
	private static final int NONE = -1;
	int oneWeek = 60*60*24*7; //Int for 1 week. :)
	int oneDay = 60*60*24; //int for 1 day
	private int DATE_DIALOG_ID = 0;
	
	private SensorManager mSensor;
	private orientListener lyssna;
	//private TextView test;
	static ImageView imv;
	int[] imSize;
    Bitmap bmp;
    static Canvas cv;
    static Paint pt;
    static int mode;  //0 = up, 1 = left, 2 = right, -1 = uninitialized
    static int tmode;
    double[] phases;
	private  List<Sensor> sensorList;
	static float[] Angs;
	static float[][] Power;
	static float[] Price;
	boolean tablet;
	static String TAG = "PWGMain";
	private float oldDist;
	private float[] start;
	double [] [] values;
	private static boolean isFirst;
	static jsonAct json;
	public int[] optPos;
	private int oldDistM;
	
	public static Context bc;
	private static boolean err;
	static int[] consLim = {0,1};
	static int[] priceLim = {0,1};
	
	
	// For LiveData
	public Runnable ld;
	public Handler handler = new Handler();

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mode = -1;
        isFirst = true;
        tablet = isTablet();
        optPos = new int[2];
        
        bc = getBaseContext();
        
        json = new jsonAct();
        
        ld = new Runnable() {
        	@Override
        	public void run() {
        		/* TODO! */
        		
        	}
        };
        handler.postDelayed(ld, (long) 500);
        
        imv = (ImageView) this.findViewById(R.id.graphView);
        
        Angs = updateAngs();
        Power = updatePower(24);
        Price = updatePrice(24);
        imSize = new int[2];
        imSize[0] = 100;
        imSize[1] = 200;
        Log.d("jving.ltu",String.valueOf(imSize[0]) +" : " + String.valueOf(imSize[1]) );
        
        bmp = Bitmap.createBitmap(imSize[0],imSize[1],Bitmap.Config.ARGB_8888);
        cv = new Canvas(bmp);
        pt = new Paint();
        pt.setColor(Color.GREEN);
        imv.setImageBitmap(bmp);

        mSensor = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        sensorList = mSensor.getSensorList(Sensor.TYPE_ACCELEROMETER);
        lyssna = new orientListener();
        mSensor.registerListener(lyssna, sensorList.get(0), SensorManager.SENSOR_DELAY_UI);
        
        
        /* For touch events */
        start = new float[2];
        start[0] = 0;
        start[1] = 1;
        
    }
	private boolean isTablet() {
		try {
			DisplayMetrics dm = this.getResources().getDisplayMetrics();
			float SW = dm.widthPixels / dm.xdpi;
			float SH = dm.heightPixels / dm.ydpi;
			double size = Math.sqrt(Math.pow(SW, 2) + Math.pow(SH, 2) );
			return size >= 5; //Tablet should have a screen larger than 5 inch
		} catch(Throwable t) {
			Log.e("jving.ltu","Failed to compute screen size",t);
			return false;
		}
	}
	@Override
	 public void onWindowFocusChanged(boolean hasFocus) {
	  // TODO Auto-generated method stub
	  super.onWindowFocusChanged(hasFocus);
	  
	  if (isFirst) {
	  //Here you can get the size!
	  imSize[0] = imv.getWidth();
      imSize[1] = imv.getHeight();
      optPos[0] = imSize[0]-70;
      optPos[1] = 70;
      if (!tablet) {
    	  optPos[1] += 38;
      }
      
      Log.d("jving.ltu",String.valueOf(imSize[0]) +" : " + String.valueOf(imSize[1]) );
      
      bmp = Bitmap.createBitmap(imSize[0],imSize[1],Bitmap.Config.ARGB_8888);
      cv = new Canvas(bmp);
      pt = new Paint();
      imv.setImageBitmap(bmp);
      
      tablet = isTablet();
      isFirst = false;
	  }

	 }
	
	@Override
	protected void onPause() {
        mSensor.unregisterListener(lyssna);
        super.onPause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mSensor.registerListener(lyssna, sensorList.get(0), SensorManager.SENSOR_DELAY_UI);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			//startActivity(new Intent(this, Prefs.class));
			
			/*DatePickerDialog.Builder build = new DatePickerDialog.Builder( this );
			build.setMessage("Select date").setTitle("Title");
			
			AlertDialog dia = build.create();
			
			dia.show(); */
			
			DatePicker datePicker = new DatePicker(this);
			//monthofYear is between 0-11
			datePicker.init(2010, 11, 1, new OnDateChangedListener() {
			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			    // Notify the user.
				Toast tst = Toast.makeText(bc, "Changed date... "+year+" "+monthOfYear+" "+dayOfMonth, Toast.LENGTH_LONG);
				tst.show();
			}
			});
			datePicker.bringToFront();
			
			Toast toast = Toast.makeText(bc, "Updating values",Toast.LENGTH_SHORT);
			toast.show();
	        json = new jsonAct();
			json.execute("2012-06-10_07:30");
	        
	        
	        
			//mode = -1; // To force update of the image
			return true;
		case R.id.menu_share:
			Bitmap icon = bmp;
			Intent share = new Intent(Intent.ACTION_SEND);
			share.setType("image/png");
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			icon.compress(Bitmap.CompressFormat.PNG, 100, bytes);
			Uri ur = Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
			File f = new File(ur.toString());
			FileOutputStream fo;
			try {
			    f.createNewFile();
			    fo = new FileOutputStream(f);
			    fo.write(bytes.toByteArray());
			    //fo.flush();
			    //fo.close();
			} catch (IOException e) {                       
			        e.printStackTrace();
			}
			share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f) );  //"file:///sdcard/temporary_file.jpg"
			startActivity(Intent.createChooser(share, "Share graph"));
			
			return true;
		}
		return false;
	}
    
    
    
    public static float[] updateAngs() {
    	/*  Method to update power consumption  */
    	float[] retval = new float[3];
    	if (isFirst) {
    		retval[0] = 1;
	    	retval[1] = 2;
	    	retval[2] = 3;
    	} else {

	    		retval[0] = (float) json.phase[0]  ;
	        	retval[1] = (float) json.phase[1]  ;
	        	retval[2] = (float) json.phase[2]  ;
    	}
    	/* testing: Random values */
    	//retval[0] = new Random().nextInt(500) + 1;
    	//retval[1] = new Random().nextInt(500) + 1;
    	//retval[2] = new Random().nextInt(500) + 1;
    	return retval;
    }
    
    
    public static float[][] updatePower(int len) {
    	/*  Method to update power consumption  */
    	float[][] retval;
    	if (isFirst) {
    		retval = new float[3][2];
    		retval[0][0] = 0;
	    	retval[1][0] = 0;
	    	retval[2][0] = 0;
	    	retval[0][1] = 1;
	    	retval[1][1] = 1;
	    	retval[2][1] = 1;
    	} else {
    		retval = json.values;//new float[3][json.getnumsteps()];
    	}
    	
    	/*for (int i = 0; i<len; i++) {
    		retval[i] = (float) new Random().nextInt(25)+5; //For now: Random values
    	}*/
    	
    	return retval;
    }
    public static float[] updatePrice(int len) {
    	/*  Method to update price */
    	float[] retval = new float[len];
    	for (int i = 0; i<len; i++) {
    		retval[i] = new Random().nextFloat(); //For now: Random values
    	}
    	return retval;
    }
    
    public int drawPie() {
    	pt.setColor(0xffcc55ee);
		
		float[] angs = new float[3];
		float total = 0;
		for (float i : Angs) {
			total += i;
		}
		angs[0] = (float) (360.0*Angs[0]/total);
		angs[1] = (float) (360.0*Angs[1]/total);
		angs[2] = (float) (360.0*Angs[2]/total);
		
		pt.setStyle(Paint.Style.STROKE);
		RectF recc = new RectF((int)(imSize[0]*0.05),(int)(imSize[0]*0.05),(int)(imSize[0]*0.95),(int)(imSize[0]*0.95));
		pt.setColor(Color.rgb(0,0,0));
		cv.drawArc(recc,(float) 0, -angs[0], true, pt);
		cv.drawArc(recc, -angs[0], -angs[1], true, pt);
		cv.drawArc(recc,-(angs[0]+angs[1]), -angs[2], true, pt);
		
		for (int i = 0; i<10; i++) {
			
			RectF rec = new RectF((int)(imSize[0]*0.05),(int)(imSize[0]*0.05-i),(int)(imSize[0]*0.95),(int)(imSize[0]*0.95-i));
			
			pt.setStyle(Paint.Style.FILL);
    		pt.setColor(Color.rgb(0+5*i,50+5*i,0+5*i));
    		cv.drawArc(rec,(float) 0, (float) -angs[0], true, pt);
    		pt.setColor(Color.rgb(50+5*i,0+5*i,0+5*i));
    		cv.drawArc(rec,(float) -angs[0], (float) -angs[1], true, pt);
    		pt.setColor(Color.rgb(0+5*i,0+5*i,50+5*i));
    		cv.drawArc(rec,(float) -(angs[0]+angs[1]), (float) -angs[2], true, pt);
		
		}
		
		pt.setStyle(Paint.Style.STROKE);
		RectF rec = new RectF((int)(imSize[0]*0.05),(int)(imSize[0]*0.05-9),(int)(imSize[0]*0.95),(int)(imSize[0]*0.95-9));
		pt.setColor(Color.rgb(0,0,0));
		cv.drawArc(rec,(float) 0, -angs[0], true, pt);
		cv.drawArc(rec, -angs[0], -angs[1], true, pt);
		cv.drawArc(rec,-(angs[0]+angs[1]), -angs[2], true, pt);
		
		//cv.drawLine(127,10,127,100,pt);
		
		pt.setTextSize((float)(imSize[0]*.05));
		pt.setStyle(Paint.Style.FILL);
		
		int cent = imSize[0]/2;
		//int[] pos = {(int) ((int) - cent*Math.sin(Math.toRadians(angs[0]/2)) + cent)   ,
		//		(int) ((int) cent*Math.cos(Math.toRadians(angs[0]/2))  + cent )  };
		int[] pos = {(int)(imSize[0]*.1),(int) (imSize[0]+imSize[0]*.1)};
		pt.setColor(Color.rgb(50,100,50));
		cv.drawText("L1: " + String.valueOf(Angs[0]) + " kWh", pos[0], pos[1], pt);
		cv.drawRect((float) (pos[0]-imSize[0]*.01),(float) pos[1],(float) (pos[0]-imSize[0]*.04),(float) (pos[1]-imSize[0]*.03), pt);
		//pos[0] = (int) ((int) - cent*Math.sin( Math.toRadians( angs[0]+angs[1]/2 ) ) + cent);
		//pos[1] = (int) ((int) cent*Math.cos(Math.toRadians(angs[0]+angs[1]/2))  + cent ) ;
		pos[1] += (int)(imSize[0]*.05);
		pt.setColor(Color.rgb(100,50,50));
		cv.drawText("L2: " + String.valueOf(Angs[1]) + " kWh", pos[0], pos[1], pt);
		cv.drawRect((float) (pos[0]-imSize[0]*.01),(float) pos[1],(float) (pos[0]-imSize[0]*.04),(float) (pos[1]-imSize[0]*.03), pt);
		//pos[0] = (int) ((int) - cent*Math.sin(Math.toRadians(angs[0]+angs[1]+angs[2]/2)) + cent) ;
		//pos[1] = (int) ((int) cent*Math.cos(Math.toRadians(angs[0]+angs[1]+angs[2]/2))  + cent ) ;
		pos[1] += (int)(imSize[0]*.05);
		pt.setColor(Color.rgb(50,50,100));
		cv.drawText("L3: " + String.valueOf(Angs[2]) + " kWh", pos[0], pos[1], pt);
		cv.drawRect((float) (pos[0]-imSize[0]*.01),(float) pos[1],(float) (pos[0]-imSize[0]*.04),(float) (pos[1]-imSize[0]*.03), pt);
		
		pt.setTextSize((float)(imSize[0]*.02));
		Date date = new Date(json.getTs(0)*1000);
		StringBuilder bd = new StringBuilder();
		bd.append(date.toString());
		bd.append(" -- ");
		date = new Date(json.getTs(-1)*1000);
		bd.append(date.toString());
		
		cv.drawText(bd.toString() ,(float) (imSize[0]*.05),(float) (imSize[1]*0.9), pt);
		
		
		imv.invalidate();
		return 0;
    }
    
    
    public int drawConsumption() {
    	pt.setColor(Color.BLACK);
  		
		cv.drawLine((float)(imSize[0]*0.05),(float)(imSize[1]*0.02),(float)(imSize[0]*0.05),(float)(imSize[1]*0.95),pt);
		cv.drawLine((float)(imSize[0]*0.05),(float)(imSize[1]*0.05),(float)(imSize[0]*0.95),(float)(imSize[1]*0.05),pt);
		float max;
		
		int[] cols = {Color.RED, Color.GREEN, Color.BLUE};
		if (isFirst) {
			max = 1;
		} else {
			max = json.maxv;
		}
		
		for (int j = 0; j < 3; j++) {
			pt.setColor(cols[j]);
			float[] ts = linspace(imSize[1]*0.05,imSize[1]*0.95,consLim[1]-consLim[0]+1);
			//Log.d(TAG,"Len: " + String.valueOf(Power.length) + ":"+ String.valueOf(Power[0].length));
			for (int i = consLim[0]; i < consLim[1]-1; i++) {
				cv.drawLine((float)( Power[j][i]/(max*1.1)*imSize[0]*0.85+imSize[0]*0.05),ts[i-consLim[0]], (float) (Power[j][i+1]/(max*1.1)*imSize[0]*0.85+imSize[0]*0.05), ts[i+1-consLim[0]], pt);
		    }
		}
		pt.setColor(Color.rgb(0,0,0));
		
		float[] xaxs = linspace(imSize[1]*0.05,imSize[1]*0.95,25);
		for (int i = 0; i < 25; i++) {
			float pl;
			if (i%6==0) {pl = (float) 0.02;} else {pl = 0;}
			cv.drawLine((float) 0.05*imSize[0], xaxs[i],(float) (0.04-pl)*imSize[0], xaxs[i], pt);
		}
		float xx = (float) (imSize[0]*(0.9/1.1+0.0)) ;
		cv.drawLine(xx, (float) (imSize[1]*.05), xx, (float) (imSize[1]*.01), pt);
		pt.setTextSize((float)(imSize[0]*.1));
		cv.rotate(90,(float) (imSize[1]*.5),(float) (imSize[0]*.5));
		pt.setColor(Color.rgb(127,127,127));
		cv.drawText("Consumption graph",(float) (imSize[1]*.35+1),(float) (imSize[0]*.4-1), pt);
		pt.setColor(Color.rgb(0,0,0));
		cv.drawText("Consumption graph",(float) (imSize[1]*.35),(float) (imSize[0]*.4), pt);
		
		
		// Draw time endpoints

		Date date = new Date(json.getTs(consLim[0])*1000);
		//Log.d(TAG,String.valueOf(json.getTs(consLim[0])));
		pt.setTextSize((float)(imSize[0]*.02));
		cv.drawText(date.toString() ,(float) (imSize[1]*.21),(float) (imSize[0]*1.2), pt);
		date = new Date(json.getTs(consLim[1])*1000);
		cv.drawText(date.toString() ,(float) (imSize[1]*.88),(float) (imSize[0]*1.2), pt);
		cv.rotate(-90,(float) (imSize[1]*.5),(float) (imSize[0]*.5));
		
		
		imv.invalidate();
		
		return 1;
    }
    
    public int drawPrice() {
    	pt.setColor(Color.BLACK);
  		
		cv.drawLine((float)(imSize[0]*0.95),(float)(imSize[1]*0.05),(float)(imSize[0]*0.95),(float)(imSize[1]*0.95),pt);
		cv.drawLine((float)(imSize[0]*0.05),(float)(imSize[1]*0.95),(float)(imSize[0]*0.95),(float)(imSize[1]*0.95),pt);
		
		pt.setColor(Color.BLUE);
		
		float max;
		Log.d(TAG,String.valueOf(json.isRun()));
		if (!json.isRun()) {
			max =(float) 1.1;
			Price = new float[2];
			Price[0] = (float) 0;
			Price[1] = (float) 1.1;
		} else {
			max = json.prices.get("Max");
			String formatter = "%02d - %02d";
			Price = new float[24];
			Object[] keys = json.prices.keySet().toArray();
			Arrays.sort(keys);
			for (int hh = 0; hh <24; hh++) {
				//String mystr = String.format(formatter, hh, hh+1);
				//Log.d(TAG,mystr + ":" + keys[hh]);
				Price[hh] = json.prices.get(keys[hh]);
			}
		}

		
		//max = maxV(Power[0]);
		float[] ts = linspace(imSize[1]*0.05,imSize[1]*0.95,Price.length);
		
		
		for (int i = 0; i < Price.length-1; i++) {
			cv.drawLine((float)( imSize[0]*0.95-Price[i]/(max*1.1)*imSize[0]*0.8),(float) (imSize[1]-ts[i]), (float) (imSize[0]*0.95-Price[i+1]/(max*1.1)*imSize[0]*0.8),(float) (imSize[1]-ts[i+1]), pt);
		}
		
		/* Draw handles */
		pt.setColor(Color.rgb(0,0,0));
		
		float[] xaxs = linspace(imSize[1]*0.05,imSize[1]*0.95,25);
		for (int i = 0; i < 25; i++) {
			float pl;
			if (i%6==0) {pl = (float) 0.02;} else {pl = 0;}
			cv.drawLine((float) 0.95*imSize[0], xaxs[i],(float) (0.97+pl)*imSize[0], xaxs[i], pt);
		}
		
		float xx = (float) (imSize[0]*(1-0.9/1.1+0.0+0.05)) ;
		cv.drawLine(xx, (float) (imSize[1]*.95), xx, (float) (imSize[1]*.99), pt);
		
		pt.setTextSize((float)(imSize[0]*.1));
		cv.rotate(-90,(float) (imSize[1]*.5),(float) (imSize[0]*.5));
		pt.setColor(Color.rgb(127,127,127));
		cv.drawText("Price graph",(float) (imSize[1]*.25+1),(float) -(imSize[0]*.1-1), pt);
		pt.setColor(Color.rgb(0,0,0));
		cv.drawText("Price graph",(float) (imSize[1]*.25),(float) -(imSize[0]*.1), pt);
		//cv.restore();
		cv.rotate(90,(float) (imSize[1]*.5),(float) (imSize[0]*.5));
		
		imv.invalidate();
		
		return 2;
    }
    
    public void doUpdate(int[] orientation) {
    	
    	
    	//Log.d("jving.ltu",String.valueOf(orientation[0])+":"+String.valueOf(orientation[1])+":"+String.valueOf(orientation[2]));
    	// Create clever function to determine rotation!
    	if (err) {
    		Toast toast = Toast.makeText(MainActivity.bc, "Update of values did not finish. Error was present",Toast.LENGTH_SHORT);
			toast.show();
			err = false;
    	}
    	
    	
    	if (orientation[0] > 6 && mode != 1 ) {
    		/* Change display to left! */
    		//draw consumption graph
    		cv.drawColor(Color.WHITE);
    		mode = drawConsumption();
    	} else if (orientation[0] < -6 && mode != 2 ) {
    		/* Change display to right! */
    		//draw price graph
    		cv.drawColor(Color.WHITE);
    		mode = drawPrice();
    	} else if (orientation[1] > 6 && mode != 0 ) {
    		/* Change display to Up! */
    		/* draw pie chart */
    		/* Should work now */
    		cv.drawColor(Color.WHITE);
    		mode = drawPie();
    	} 
    	if (true) {
    		cv.drawColor(Color.WHITE);
    		switch (mode) {
    		case 0:
    			drawPie();
    			break;
    		case 2:
    			drawPrice();
    			break;
    		case 1:
    			drawConsumption();
    			break;
    		}
    		
    	}
    	//test.append(String.valueOf(orientation[0])+":"+String.valueOf(orientation[1])+":"+String.valueOf(orientation[2]));
    	//test.invalidate();
    	
    	
    	
    }
    
    
    private float[] linspace(double start, double end, int length) {
		float[] retval = new float[length];
    	float stepp = (float) ((end-start)/(length-1));
    	for (int i = 0; i<length; i++) {
    		retval[i] = (float) (start + ((float)i)*stepp);
    	}
		
		
		return retval;
	}


	public class orientListener implements SensorEventListener{

		private float X;
		private float Y;
		private float Z;

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			// TODO Auto-generated method stub
			if (tablet) {
				X = event.values[1];
				Y = -event.values[0];
			} else {
				X = event.values[0];
				Y = event.values[1];	
			}
			Z = event.values[2];
			//Log.d("jving.ltu",String.valueOf( (int) X ));
			int[] retval = {(int) X,(int) Y,(int) Z};
			
			
			doUpdate(retval);
		}
    	
    }
    
    public float maxV(float[] vect){
    	float retval = 0;
    	for (float i : vect) {
    		if (i > retval) { retval = i; }
    	}
    	
    	
    	return retval;
    }
    @Override
	public boolean dispatchTouchEvent(MotionEvent event) {
    	/*  Steals "settings"-click =/  */
    	 //dumpEvent(event);
    	 switch (event.getAction() & MotionEvent.ACTION_MASK) {
         case MotionEvent.ACTION_DOWN:
             //savedMatrix.set(matrix);
             start[0] = event.getX();
             start[1] = event.getY();
             oldDistM = 0;
             //Log.d(TAG, "mode=DRAG");
             tmode = DRAG;
             break;
         case MotionEvent.ACTION_POINTER_DOWN:
             oldDist = spacing(event);
             Log.d(TAG, "oldDist=" + oldDist);
             if (oldDist > 10f) {
                 //savedMatrix.set(matrix);
                 //midPoint(mid, event);
                 tmode = ZOOM;
                 Log.d(TAG, "mode=ZOOM");
             }
             break;
         case MotionEvent.ACTION_UP:
        	 if (start[0] > optPos[0] && start[1] < optPos[1] && event.getX() > optPos[0] && event.getY() < optPos[1]) {
        		 openOptionsMenu();
        	 }
         case MotionEvent.ACTION_POINTER_UP:
             tmode = NONE;
             //Log.d(TAG, "mode=NONE");
             break;
         case MotionEvent.ACTION_MOVE:
             if (tmode == DRAG) {
                 // ...
            	 double scale = 0.9*imSize[1]/(consLim[1]-consLim[0]); 
            	 //scale = 1; // does not function as I want...
            	 int dist = (int) (event.getY() - start[1]);
            	 if (dist != oldDistM) {
            		 // Move limits
            		 
            		 if ((consLim[0] - dist/scale) >= 0 && (consLim[1] - dist/scale) <= json.getnumsteps()-1 && mode == 1 && Math.abs(dist/scale) >= 1) {
            			 consLim[0] -= dist/scale;
            			 consLim[1] -= dist/scale;
            			 cv.drawColor(Color.WHITE);
            			 drawConsumption();
            			 start[1] = event.getY();
            		 }
            		 //Log.d(TAG,String.valueOf(consLim[0])+":"+String.valueOf(consLim[1]));
            		 oldDistM = dist;
            	 }
            	 
                 
             } else if (tmode == ZOOM) {
                 float newDist = spacing(event);
                 float scale = 2;
                 //Change limits.
                 float diff = oldDist - newDist;
                 Log.d(TAG, "newDist=" + newDist + "; diff=" + diff);
                 if (diff > scale || diff < -scale) {
                	 int resi =(int) (diff/scale);
                	 if (consLim[0]-resi >= 0 && consLim[1]-consLim[0]-2*resi >= 10) {
                		 consLim[0] -= resi;
                	 }
                	 if (consLim[1]+resi <= json.getnumsteps()-1 && consLim[1]-consLim[0]-2*resi >= 10) {
                		 consLim[1] += resi;
                	 }
                	 oldDist = newDist;
                	 
                 }
                 
             }
             break;
         }
    	
    	 
    	return true;
    }
   

    private void dumpEvent(MotionEvent event) {
        String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE",
                "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN
                || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(
                    action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }
        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }
        sb.append("]");
        Log.d(TAG, sb.toString());
    }

    /** Determine the space between the first two fingers */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt(x * x + y * y);
    }

    /** Calculate the mid point of the first two fingers */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }
	public static void updateAllValues() {
		// Use this method to update the values for the graphs....
        //Log.d(TAG,"Timesteps: " + String.valueOf(json.getnumsteps()));
		
        if (json.getnumsteps() == 0) {
    		err = true;
        } else {
        	consLim[0] = 0;//(int)(json.getnumsteps()*.9);
        	consLim[1] = (int)(json.getnumsteps()-1);
        	// Still for random data...
        	priceLim[0] = (int)(48);
        	priceLim[1] = (int)(72);
	        Angs = updateAngs();
			Power = updatePower(24*3);
	        Price = updatePrice(24*3);
        }
		
	}
    
	
	
/*	protected Dialog onCreateDialog(int id) {
		Calendar c = Calendar.getInstance();
		int cyear = c.get(Calendar.YEAR);
		int cmonth = c.get(Calendar.MONTH);
		int cday = c.get(Calendar.DAY_OF_MONTH);
		switch (id) {
		case DATE_DIALOG_ID :
		return new DatePickerDialog(this,  updateJson,  cyear, cmonth, cday);
		}
		return null;
		}
	void updateJson() {
		
	}*/
	@TargetApi(11)
	public static class TimePickerFragment extends DialogFragment
    implements DatePickerDialog.OnDateSetListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day= c.get(Calendar.DAY_OF_MONTH);
	
	// Create a new instance of TimePickerDialog and return it
	return new DatePickerDialog(bc, this, year, month,day);
	}
	
	public void onDateSet(DatePicker view, int year, int month, int day) {
		// Do something with the time chosen by the user
	}
}


}