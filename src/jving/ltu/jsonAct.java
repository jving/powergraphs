package jving.ltu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.os.AsyncTask;
import android.util.Log;

public class jsonAct extends AsyncTask<String, Void, Boolean> {
	
	public float[][] values;
	String baseurl = "http://www.eislab.com/weather/get_data.php?s=2013-01-01_00:00";
	String baseurl2 = "http://www.eislab.com/weather/get_data.php?s=";
	String priceUrl = "http://www.nordpoolspot.com/Market-data1/Elspot/Area-Prices/ALL1/Hourly/";
	String TAG = "jsonAct";
	public int[] tsteps;
	public double[] phase;
	public float maxv = 1;
	public HashMap<String,Float> prices = new HashMap<String,Float>();
	private String dformat = "yyyy-MM-dd'_'HH:mm";

	//private ProgressDialog dialog;
	
	//public jsonAct() {
		//dialog = new ProgressDialog(Context.CONTEXT_RESTRICTED);
	//}

			
	@Override
	protected Boolean doInBackground(String... dateTime) {
	    	/*  Method to fetch data from web and parse the json results  */
	    	/*  Currently uses tempData  */
	    	// First value: 2012-06-10_07:30
		
		SimpleDateFormat sdf = new SimpleDateFormat(dformat, Locale.ENGLISH);
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.DATE, -1); 
    	
    	Log.d(TAG,"http://www.eislab.com/weather/get_data.php?s=2012-09-20_23:30");
    	Log.d(TAG,baseurl2 + sdf.format(cal.getTime()));
		

	    	StringBuilder builder = new StringBuilder();
	    	HttpClient client = new DefaultHttpClient();
	    	HttpGet httpGet = new HttpGet(baseurl2 + sdf.format(cal.getTime()));//+dateTime); // Maybe do a sanity check?
	    	
			try {
		    	HttpResponse response = client.execute(httpGet);
		        StatusLine statusLine = response.getStatusLine();
		        int statusCode = statusLine.getStatusCode();
		        if (statusCode == 200) {
		          HttpEntity entity = response.getEntity();
		          InputStream content;
				  content = entity.getContent();
				  BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		          String line;
				  while ((line = reader.readLine()) != null) {
					    builder.append(line);
				  }
		        } else {
		            Log.e(TAG, "Failed to download consumption");
		            Log.e(TAG, statusCode + '\n' + statusLine.getReasonPhrase());
		        }
		        /*if (stl.getStatusCode() == 200) {
		        	HttpEntity ent2 = resp2.getEntity();
		        	InputStream content;
					content = ent2.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						    builder2.append(line);
					 } 
		        	
		        } else {
		        	Log.e(TAG, "Failed to download price");
		        }*/
		        
		        
		        // Jsoup method
		        Document sidan = Jsoup.connect(priceUrl).get();
		        Elements els = sidan.select("div#ctl00_FullRegion_npsGridView_trkGrid tr td");
		        //Log.d(TAG, "els: " + els.get(0) + "\nels: " + els.get(1) + "\nels: " + els.get(2));
		        for (int i=4; i < els.size() ; i += 13) {
		        	//Log.d(TAG, String.valueOf(els.get(i-1).text().length()) );
		        	if (!( els.get(i-1).text().equalsIgnoreCase("sys") || els.get(i-1).text().length() < 3 )) { 
		        		String key = els.get(i-1).text();
		        		String val = els.get(i).text();
		        		//Log.d(TAG, val);
		        		val = val.replace(",", ".");
		        		//Log.d(TAG, val);
		        		Float value = Float.parseFloat(   val   );
		        	    prices.put(key, value);
		        	    Log.d(TAG,"key: " + String.valueOf(key) + ", value: " + String.valueOf(value) );
		        	}
		        	//Log.d(TAG, "els -- " + els.get(i-1).text() + " : " + els.get(i).text());
		        	
		        }
		        
		        Log.d(  TAG,"test: " + String.valueOf( prices.get(  ( prices.keySet().toArray()[0]  )  ) )  );
		        Log.d(  TAG,"test: " + String.valueOf( prices.get("Max") )  );
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	//String tempData = "{\"error\":false,\"values\":[{\"timestamp\":1346603989,\"1\":0.1519,\"2\":0.8019,\"3\":0},{\"timestamp\":1346604054,\"1\":0.1519,\"2\":0.8069,\"3\":0},{\"timestamp\":1346604247,\"1\":0.1519,\"2\":0.804,\"3\":0},{\"timestamp\":1346604312,\"1\":0.1519,\"2\":0.8119,\"3\":0},{\"timestamp\":1346604376,\"1\":0.1529,\"2\":0.8059,\"3\":0},{\"timestamp\":1346604440,\"1\":0.1519,\"2\":0.813,\"3\":0},{\"timestamp\":1346604474,\"1\":0.1789,\"2\":0.8019,\"3\":0},{\"timestamp\":1346604539,\"1\":0.178,\"2\":0.903,\"3\":0},{\"timestamp\":1346604735,\"1\":0.177,\"3\":0},{\"timestamp\":1346604866,\"2\":0.798,\"3\":0,\"1\":0.5559},{\"timestamp\":1346604995,\"2\":0.8019,\"3\":0,\"1\":0.1519},{\"timestamp\":1346605061,\"3\":0,\"1\":0.1519,\"2\":0.8},{\"timestamp\":1346605127,\"3\":0,\"1\":0.1529,\"2\":0.8019}]}";
	    	double[] valus = new double[3];
	    	try {
				JSONObject jsonO = new JSONObject(builder.toString());
				//Log.d(TAG,"Number of entries " + jsonO.length());
				JSONArray jsoArr = jsonO.getJSONArray("values");  
				tsteps = new int[jsoArr.length()];
				values = new float[3][jsoArr.length()];
				phase = new double[3];
				phase[0] = 0;
				phase[1] = 0;
				phase[2] = 0;
				for (int i = 0; i< jsoArr.length(); i++ ) {
					JSONObject jso = jsoArr.getJSONObject(i);
					//Log.d(TAG,"  Json: " + jso.optString("timestamp") + "," + String.valueOf(jso.optDouble("1")) + "," + String.valueOf(jso.optDouble("2")) + "," + String.valueOf(jso.optDouble("3")));
					tsteps[i] = jso.optInt("timestamp");
					valus[0] = (double) jso.optDouble("1");
					valus[1] = (double) jso.optDouble("2");
					valus[2] = (double) jso.optDouble("3");
					for (int j = 0; j < 3; j++) {
						if ( Double.isNaN(valus[j]) ) { //Not detecting NaN... GAH!
							valus[j] = 0;
							//Log.d(TAG,"Got NaN");
						}
					}
					if (i > 1) {
						phase[0] += valus[0]*(tsteps[i]-tsteps[i-1])/60/60; // Get it into kWh
						phase[1] += valus[1]*(tsteps[i]-tsteps[i-1])/60/60; // Get it into kWh
						phase[2] += valus[2]*(tsteps[i]-tsteps[i-1])/60/60; // Get it into kWh
					}
					values[0][i] = (float) valus[0];
					values[1][i] =(float) valus[1];
					values[2][i] =(float) valus[2];
					
					maxv = (float) Math.max(valus[0], maxv);
					maxv = (float) Math.max(valus[1], maxv);
					maxv = (float) Math.max(valus[2], maxv);
					}
				//Log.d(TAG,"Phases: " + String.valueOf(phase[0]) + "," + String.valueOf(phase[1]) + "," + String.valueOf(phase[2]) + ",");
			} catch (JSONException je) {
				// TODO Auto-generated catch block
				je.printStackTrace();
			}
	    	
	    	MainActivity.updateAllValues();
	    	return true;
	    

	}
	
	protected void onPostExecute() {
		//MainActivity.updateAllValues();
		
	}

	public int getnumsteps() {
		// TODO Auto-generated method stub
		if (tsteps == null) {
			return 0;
		} else { 
			return tsteps.length;
		}
	}

	public Long getTs(int i) {
		// TODO Auto-generated method stub
		if (tsteps == null) {
			return (long) 0;
		} else { 
			if (i == -1) {
				return (long) tsteps[tsteps.length-1];
			} else {
				return (long) tsteps[i];
			}
		}
	}

	public boolean isRun() {
		// TODO Auto-generated method stub
		if (tsteps == null) {
		    return false;
		} else {
			return true;
		}
	}
	

}